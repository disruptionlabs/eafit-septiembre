# Use version of Ruby 2.2
FROM ruby:2.2

# Optionally set a maintainer name to let people know who made this image.
MAINTAINER Disruption LABS <info@disruptionlabs.co>

# Install dependencies:
# - build-essential: To ensure certain gems can be compiled
# - nodejs: Compile assets
RUN apt-get update && apt-get install -qq -y build-essential nodejs

ENV RAILS_ENV production

# Set an environment variable to store where the app is installed to inside
# of the Docker image.
ENV INSTALL_PATH /eafit_demo
RUN mkdir -p $INSTALL_PATH

# This sets the context of where commands will be ran in and is documented
# on Docker's website extensively.
WORKDIR $INSTALL_PATH

# Ensure gems are cached and only get updated when they change. This will
# drastically increase build times when your gems do not change.
COPY Gemfile Gemfile
COPY Gemfile.lock Gemfile.lock
RUN bundle install --without development test

# Copy in the application code from your work station at the current directory
# over to the working directory.
COPY . .

# Provide dummy data to Rails so it can pre-compile assets.
RUN bundle exec rake MONGOLAB_URI="mongodb://dummy:27017/dummy_db" \
SECRET_KEY_BASE=dummy assets:precompile

# Expose a volume so that nginx will be able to read in assets in production.
VOLUME ["$INSTALL_PATH/public"]

# The default command that gets ran will be to start the Unicorn server.
CMD bundle exec puma -C config/puma.rb