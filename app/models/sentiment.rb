class Sentiment
  include Mongoid::Document

  # ===
  # Fields
  # ===
  field :type, type: String
  field :score, type: String
  field :mixed, type: String

  # ===
  # Associations
  # ===
  embedded_in :tweet
end
