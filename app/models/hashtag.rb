class Hashtag
  include Mongoid::Document

  # ===
  # Fields
  # ===
  field :hashtag, type: String

  # ===
  # Associations
  # ===
  has_many :tweets

  # ===
  # Methods
  # ===
  def has_tweet?(twitter_id)
    self.tweets.where(twitter_id: twitter_id).count > 0
  end

  def save_tweet(tweet)
    tweet = self.tweets.build text: tweet.full_text,
                              twitter_id: tweet.id.to_s,
                              url: tweet.url.to_s,
                              author_id: tweet.user.id.to_s

    tweet.save
  end

  def tweet_with_id(twitter_id)
    self.tweets.where(twitter_id: twitter_id).first
  end

  def un_analyzed_tweets
    self.tweets.where(analyzed: false)
  end

  def has_tweets_for_analysis?
    self.tweets.where(analyzed: false).count > 0
  end

  def google_charts_json
    sentiment_dictionary = {}

    self.tweets.each do |tweet|
      # Value is an array: first element is the amount of tweets with that sentiment, second is the sum of it's scores
      if tweet.sentiment
        if sentiment_dictionary.has_key? tweet.sentiment.type
          sentiment_dictionary[tweet.sentiment.type] = [
              sentiment_dictionary[tweet.sentiment.type].first + 1,
              sentiment_dictionary[tweet.sentiment.type].last + tweet.sentiment.score.to_f
          ]
        else
          sentiment_dictionary[tweet.sentiment.type] = [0, tweet.sentiment.score.to_f]
        end
      end
    end

    data = sentiment_dictionary.collect do |key, value|
      ["#{key} (#{value.last / value.first})", value.first] if value.first != 0
    end

    data.compact.unshift(['Sentiment', 'Amount of tweets']).to_json
  end
end
