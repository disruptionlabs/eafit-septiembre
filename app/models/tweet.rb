class Tweet
  include Mongoid::Document

  # ===
  # Fields
  # ===
  field :text, type: String
  field :twitter_id, type: String
  field :url, type: String
  field :author_id, type: String
  field :analyzed, type: Mongoid::Boolean, default: false

  # ===
  # Associations
  # ===
  belongs_to :hashtag
  embeds_one :sentiment
  embeds_many :taxonomies

  # ===
  # Methods
  # ===
  def marked_as_analyzed
    self.analyzed = true
    self.save
  end
end
