class Taxonomy
  include Mongoid::Document

  # ===
  # Fields
  # ===
  field :confident, type: String
  field :label, type: String
  field :score, type: String

  # ===
  # Associations
  # ===
  embedded_in :tweet
end
