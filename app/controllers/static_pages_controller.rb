class StaticPagesController < ApplicationController

  # ===
  # Actions
  # ===
  def index
    redirect_to hashtags_path
  end
end
