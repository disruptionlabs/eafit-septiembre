class HashtagsController < ApplicationController
  include TwitterClientHelper, AlchemyHelper

  before_action :set_hashtag, only: [:show, :sync_tweets, :tweets, :analyze_tweets]

  # ===
  # Actions
  # ===
  def index
    @hashtags = Hashtag.all
  end

  def new
    @hashtag = Hashtag.new
  end

  def create
    @hashtag = Hashtag.new(hashtag_params)

    respond_to do |format|
      if @hashtag.save
        format.html { redirect_to hashtags_path, notice: 'Hashtag was successfully created.' }
      else
        format.html { render :new }
      end
    end
  end

  def show
  end

  def sync_tweets
    tweets = get_tweets_for_hashtag(@hashtag.hashtag)
    tweets.each do |tweet|
      unless @hashtag.has_tweet? tweet.id.to_s
        @hashtag.save_tweet tweet
      end
    end

    redirect_to tweets_hashtag_path(@hashtag)
  end

  def analyze_tweets
    @hashtag.un_analyzed_tweets.each do |tweet|
      analyze_tweet tweet
    end

    redirect_to tweets_hashtag_path @hashtag
  end

  def tweets
    @tweets = @hashtag.tweets
  end

  private
  def set_hashtag
    @hashtag = Hashtag.find(params[:id])
  end

  def hashtag_params
    params.require(:hashtag).permit(:hashtag)
  end
end
