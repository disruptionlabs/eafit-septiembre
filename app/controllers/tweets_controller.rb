class TweetsController < ApplicationController
  include AlchemyHelper

  before_action :set_tweet

  # ===
  # Actions
  # ===
  def analyze
    analyze_tweet @tweet
    redirect_to @tweet
  end

  def show
  end

  private
  def set_tweet
    @tweet = Tweet.find(params[:id])
  end
end
