module TwitterClientHelper
  def get_tweets_for_hashtag(hashtag)
    twitter_client.search("##{hashtag} -rt")
  end

  private
  def twitter_client
    Twitter::REST::Client.new do |config|
      config.consumer_key        = ENV['CONSUMER_KEY']
      config.consumer_secret     = ENV['CONSUMER_SECRET']
      config.access_token        = ENV['TOKEN']
      config.access_token_secret = ENV['TOKEN_SECRET']
    end
  end
end
