module AlchemyHelper
  def analyze_tweet(tweet)
    get_sentiment_for_tweet tweet
    get_taxonomies_for_tweet tweet

    tweet.marked_as_analyzed
  end

  private
  def get_sentiment_for_tweet(tweet)
    url = get_alchemy_query_url(tweet.text, 'text/TextGetTextSentiment')
    response = RestClient.get url, { content_type: :json, accept: :json }
    json_response = JSON.parse response.body

    tweet.create_sentiment json_response['docSentiment'] if json_response['docSentiment']
  end

  def get_taxonomies_for_tweet(tweet)
    url = get_alchemy_query_url(tweet.text, 'text/TextGetRankedTaxonomy')
    response = RestClient.get url, { content_type: :json, accept: :json }
    json_response = JSON.parse response.body

    if json_response['taxonomy']
      json_response['taxonomy'].each do |taxonomy|
        tweet.taxonomies.create taxonomy
      end
    end
  end

  def get_alchemy_query_url(text, alchemy_resource)
    encoded_text = URI.encode text
    "#{ENV['ALCHEMY_URL']}/#{alchemy_resource}?apikey=#{ENV['ALCHEMY_API_KEY']}&outputMode=json&text=#{encoded_text}"
  end
end
