module HashtagsHelper
  def twitter_hashtag_url(hashtag)
    "https://twitter.com/search?q=%23#{hashtag.hashtag}"
  end
end
