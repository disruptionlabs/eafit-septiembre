Rails.application.routes.draw do
  get 'tweet/analyze'

  get 'tweet/show'

  # ===
  # Root
  # ===
  root 'static_pages#index'

  resources :hashtags, only: [:index, :new, :create, :show] do
    member do
      post 'sync_tweets'
      post 'analyze_tweets'
      get 'tweets'
    end
  end

  resources :tweets, only: [:show] do
    member do
      post 'analyze'
    end
  end
end
